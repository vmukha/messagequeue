##MessageQueue library
###Version 1.0.0
###Introduction
This library is a C++ implementation of a message queue class with thread-safe operations to enqueue and dequeue messages.
It is implemented using C++11 standard.

###License
It is licensed under MIT license. See [LICENSE](../../LICENSE) file for details.

###Documentation
See ["doc/html/index.html"](index.html) for doxygen-generated documentation.

###Build dependencies
* C++ compiler which supports C++11 standard (currently tested with gcc 6.3.0)
* [bake](https://esrlabs.github.io/bake/install/install_bake.html) >= 2.38.3
* To build tests one has to install [Google Test](https://github.com/google/googletest)

###How to build

To build a static library only:
```
bake Lib
```

To build both static library and tests:
```
bake --set GTEST_DIR="/your/path/to/googletest" UnitTestBase
```

To run these tests:
```
./build/UnitTestBase/messagequeue
```

###Performance
Current implementation of message queue internally uses std::deque, which yeilds following runtimes:
* **enqueue** -- _O(1)_ worst case
* **dequeue** -- _O(1)_ worst case
* **getFirstMessage** -- _O(1)_ worst case
* **deleteSpecificMessages** -- _O(n)_ -- number of calls to destructor of Message is the number of elements being erased.
Current implementation uses [erase-remove idiom](https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom).
Since Message is [MoveAssignable](http://en.cppreference.com/w/cpp/concept/MoveAssignable), we can expect that all Messages are moved and not copied.

Future patch versions of the library can improve running times without changing the interface. For versioning politics please refer to [Semantic Versioning Specification 2.0.0](http://semver.org/spec/v2.0.0.html).
