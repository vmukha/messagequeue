var searchData=
[
  ['messagequeue_20library',['MessageQueue library',['../index.html',1,'']]],
  ['message',['Message',['../structMessage.html',1,'Message'],['../structMessage.html#a4fc4f717b634e66070366cb7722d7761',1,'Message::Message()'],['../structMessage.html#a4c6be5182b54297fbeb56797e4fedce0',1,'Message::Message(int _what, int _arg1, int _arg2, void *_obj)']]],
  ['message_2eh',['Message.h',['../Message_8h.html',1,'']]],
  ['message_5fqueue_5fapi_2eh',['MESSAGE_QUEUE_API.h',['../MESSAGE__QUEUE__API_8h.html',1,'']]],
  ['messagequeue',['MessageQueue',['../classMessageQueue.html',1,'MessageQueue'],['../classMessageQueue.html#a53a502b48145aaa8ed8b6795da65209d',1,'MessageQueue::MessageQueue()'],['../classMessageQueue.html#a23b6bdd12149821af404aa79a5b31ed7',1,'MessageQueue::MessageQueue(const MessageQueue &amp;)=delete'],['../classMessageQueue.html#a08b172a0ef0ac7f5766015c91ff32135',1,'MessageQueue::MessageQueue(MessageQueue &amp;&amp;)=delete']]],
  ['messagequeue_2eh',['MessageQueue.h',['../MessageQueue_8h.html',1,'']]]
];
