// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file MessageQueueImpl.cpp
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Actual implementation of MessageQueue class
 */

#include "MessageQueueImpl.h"

#include <algorithm> // std::remove

MessageQueueImpl::MessageQueueImpl() = default;
MessageQueueImpl::~MessageQueueImpl() = default;

void MessageQueueImpl::enqueue( const Message& msg )
{
	{
		std::unique_lock< std::mutex > lock( m_mutex );
		m_queue.emplace_back( msg );
	}
	m_cond.notify_one();
}

void MessageQueueImpl::enqueue( Message&& msg )
{
	{
		std::unique_lock< std::mutex > lock( m_mutex );
		m_queue.emplace_back( std::move( msg ) );
	}
	m_cond.notify_one();
}

Message MessageQueueImpl::dequeue()
{
	std::unique_lock< std::mutex > lock( m_mutex );
	m_cond.wait( lock, [ this ]() { return !m_queue.empty(); } );
	Message msg = std::move( m_queue.front() );
	m_queue.pop_front();
	return msg;
}

bool MessageQueueImpl::getFirstMessage( Message& msg ) const
{
	std::unique_lock< std::mutex > lock( m_mutex );
	if ( m_queue.empty() )
		return false;
	// copy the message
	msg = m_queue.front();
	return true;
}

void MessageQueueImpl::deleteSpecificMessages( int what )
{
	std::unique_lock< std::mutex > lock( m_mutex );
	// using erase-remove idiom
	m_queue.erase( std::remove_if( m_queue.begin(), m_queue.end(), [ what ]( const Message& m ){ return m.what == what; } ),
	               m_queue.end() );
}
