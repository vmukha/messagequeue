// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file MessageQueueImpl.h
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Actual implementation of MessageQueue class
 */

#ifndef _MESSAGEQUEUEIMPL_H_
#define _MESSAGEQUEUEIMPL_H_

#include "Message.h"

#include <mutex>
#include <condition_variable>
#include <deque>

class MessageQueueImpl final
{
public:
	/// default constructor
	explicit MessageQueueImpl();
	
	/// rule of zero won't work due to pimpl, so using rule of five
	~MessageQueueImpl();
	
	/// copying existing message queue is usually not what we want
	MessageQueueImpl( const MessageQueueImpl& ) = delete;
	MessageQueueImpl& operator=( const MessageQueueImpl& ) = delete;
	
	/// moves would be ill-formed in context of mutex
	MessageQueueImpl( MessageQueueImpl&& ) = delete;
	MessageQueueImpl& operator=( MessageQueueImpl&& ) = delete;
	
	/// push new message into the queue
	/// @param[ in ] msg Message to be put into queue
	/// 
	void enqueue( const Message& msg );
	void enqueue( Message&& msg );
	
	/// pop oldest message from the queue
	/// @returns dequeued message
	///
	Message dequeue();
	
	/// get value of first element in the queue
	/// @param[ out ] msg Message to write the contents of first message to
	/// @returns false if queue is empty, true otherwise
	/// 
	/// The method does not wait until messages appear in the queue, but asks for currently first message.
	/// 
	/// NOTE: not giving reference or const-reference since it is error-prone is multi-threaded context.
	///       Also, modifying the first message of a message queue is not what classic producer-consumer want
	/// 
	bool getFirstMessage( Message& msg ) const;
	
	/// delete all elements with a specific "what" field from the queue
	/// @param what deletion criterion
	/// 
	/// This method does one-shot blocking deletion since when it is called we don't want 
	/// any other consumers to get these messages anymore
	void deleteSpecificMessages( int what );

private:
	/// syncronization primitives
	mutable std::mutex m_mutex;
	mutable std::condition_variable m_cond;
	
	/// actual data structure to store messages
	std::deque< Message > m_queue;
};


#endif // _MESSAGEQUEUEIMPL_H_
