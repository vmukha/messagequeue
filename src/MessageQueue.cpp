// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file MessageQueue.cpp
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Forwarding implementation to impl class
 */

#include "MessageQueue.h"
#include "MessageQueueImpl.h"

MESSAGE_QUEUE_API MessageQueue::MessageQueue()
    : m_pimpl( new MessageQueueImpl() )
{}

MESSAGE_QUEUE_API MessageQueue::~MessageQueue() = default;

/// forwarding methods to m_pimpl without additional changes here

MESSAGE_QUEUE_API void MessageQueue::enqueue( const Message& msg ) { m_pimpl->enqueue( msg ); }
MESSAGE_QUEUE_API void MessageQueue::enqueue( Message&& msg ) { m_pimpl->enqueue( std::move( msg ) ); }

/// RVO should take care of avoiding the copy
MESSAGE_QUEUE_API Message MessageQueue::dequeue() { return m_pimpl->dequeue(); }
MESSAGE_QUEUE_API bool MessageQueue::getFirstMessage( Message& msg ) const { return m_pimpl->getFirstMessage( msg ); }

MESSAGE_QUEUE_API void MessageQueue::deleteSpecificMessages( int what ) { m_pimpl->deleteSpecificMessages( what ); }
