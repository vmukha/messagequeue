// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file ProducerConsumer.h
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Producer and Consumer classes for MessageQueue testing
 */

#ifndef _PRODUCERCONSUMER_H_
#define _PRODUCERCONSUMER_H_

#include "MessageQueue.h"

#include <string>
#include <thread>
#include <vector>
#include <iostream>
#include <memory>

//#define PRODUCER_CONSUMER_COUT

#ifdef PRODUCER_CONSUMER_COUT
#include <mutex>
std::mutex coutMutex; // mutex for std::cout to get sensible output
#endif

/// @class Producer
/// @brief produces messages and enqueues them
class Producer
{
public:
	/// constructor
	/// @param[ in, out ] queue Message queue which is assumed to have thread-safe enqueue method
	/// @param[ in ] objectPool pool of objects to embed into messages, it's size determines the number of messages
	/// @param[ in ] (optional) sleepAfter duration of sleep after producing (default 0)
	/// @param[ out ] (optional) produced vector of produced items (default nullptr)
	explicit Producer( MessageQueue& queue, std::vector< std::string >& objectPool,
	                   std::chrono::duration< int, std::milli > sleepAfter = std::chrono::milliseconds( 0 ),
	                   std::shared_ptr< std::vector< Message > > produced = nullptr )
	    : m_queue( queue )
	    , m_objectPool( objectPool )
	    , m_sleepAfter( sleepAfter )
	    , m_produced( produced )
	{}
	
	/// method which runs in producer thread
	void run()
	{
		for ( size_t i = 0; i < m_objectPool.size(); ++i )
		{
			// produce message
			Message msg( i % 10, i, 42, &( m_objectPool[ i ] ) );
			m_queue.enqueue( msg );
			
			if ( m_produced )
				m_produced->emplace_back( msg );
			
#ifdef PRODUCER_CONSUMER_COUT
			// tell the world about it
			{
				std::unique_lock< std::mutex > lock( coutMutex );
				std::cout << std::this_thread::get_id() << " produced " << m_objectPool.at( i ) << "\n";
			}
#endif
			
			// sleep just enough to check if consumers are impatient and die from waiting
			std::this_thread::sleep_for( m_sleepAfter );
		}
	}
	
private:
	MessageQueue& m_queue;
	std::vector< std::string >& m_objectPool;
	
	/// duration of sleep in ms
	std::chrono::duration< int, std::milli > m_sleepAfter;
	
	/// vector of produced messages
	std::shared_ptr< std::vector< Message > > m_produced;
};

class Consumer
{
public:
	/// constructor
	/// @param[ in, out ] queue Message queue which is assumed to have thread-safe dequeue method
	/// @param[ in ] numOfMessages number of messages consumer must process.
	///                            When it is reached, cosumer will not wait for new messages anymore.
	/// @param[ in ] (optional) sleepAfter duration of sleep after consuming (default 0)
	/// @param[ out ] (optional) consumed vector of consumed items (default nullptr)
	explicit Consumer( MessageQueue& queue, size_t numOfMessages,
	                   std::chrono::duration< int, std::milli > sleepAfter = std::chrono::milliseconds( 0 ),
	                   std::shared_ptr< std::vector< Message > > consumed = nullptr )
	    : m_queue( queue )
	    , m_numOfMessages( numOfMessages )
	    , m_sleepAfter( sleepAfter )
	    , m_consumed( consumed )
	{}
	
	/// method which runs in consumer thread
	void run()
	{
		for ( size_t i = m_numOfMessages; i > 0; --i )
		{
			// consume next message
//			Message msg = m_queue.getFirstMessage();
			Message msg = m_queue.dequeue();
			
			if ( m_consumed )
				m_consumed->emplace_back( msg );
			
#ifdef PRODUCER_CONSUMER_COUT
			// tell the world about it
			{
				std::unique_lock< std::mutex > lock( coutMutex );
				std::cout << std::this_thread::get_id() << " consumed Message(" <<
				             msg.what << ", " <<
				             msg.arg1 << ", " <<
				             msg.arg2 << ", \"" <<
				             *( static_cast< std::string* >( msg.obj ) ) << "\")\n";
			}
#endif
			
			std::this_thread::sleep_for( m_sleepAfter );
		}
	}
	
private:
	MessageQueue& m_queue;
	size_t m_numOfMessages;
	
	/// duration of sleep in ms
	std::chrono::duration< int, std::milli > m_sleepAfter;
	
	/// vector of consumed messages
	std::shared_ptr< std::vector< Message > > m_consumed;
};

#endif // _PRODUCERCONSUMER_H_
