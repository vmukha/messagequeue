// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file test.cpp
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Tests for MessageQueue class
 */

#include "MessageQueue.h"
#include "ProducerConsumer.h"

#include <string>
#include <gtest/gtest.h>

/// helper to check if queue is empty after several operations on it
static bool isQueueEmpty( MessageQueue& queue )
{
	Message m;
	return ( ! queue.getFirstMessage( m ) );
}

TEST( Message, DefaultConstructMessage )
{
	Message m;
	EXPECT_EQ( 0, m.arg1 );
	EXPECT_EQ( 0, m.arg2 );
	EXPECT_EQ( 0, m.what );
	EXPECT_EQ( nullptr, m.obj );
}

TEST( Message, ConstructMessage )
{
	std::string obj( "test object" );
	Message m( 1, 2, 3, &obj );
	EXPECT_EQ( 1, m.what );
	EXPECT_EQ( 2, m.arg1 );
	EXPECT_EQ( 3, m.arg2 );
	EXPECT_EQ( m.obj, &obj );
}

TEST( MessageQueue, DefaultConstructMessageQueue )
{
	ASSERT_NO_THROW( MessageQueue mq; );
}

/// 
/// single-threaded tests of MessageQueue

TEST( MessageQueue, EnqueueMessageLValue )
{
	ASSERT_NO_THROW( MessageQueue mq; );
	MessageQueue mq;
	Message msg;
	ASSERT_NO_THROW( mq.enqueue( msg ) );
}

TEST( MessageQueue, EnqueueMessageRValue )
{
	ASSERT_NO_THROW( MessageQueue mq; );
	MessageQueue mq;
	ASSERT_NO_THROW( mq.enqueue( Message{} ) );
}

TEST( MessageQueue, EnqueueDequeue )
{
	ASSERT_NO_THROW( MessageQueue mq; );
	MessageQueue mq;
	std::string obj( "test object" );
	Message initialMessage( 1, 2, 3, &obj );
	Message dequeuedMessage;
	ASSERT_NO_THROW( mq.enqueue( initialMessage ) );
	
	try {
		dequeuedMessage = mq.dequeue();
	} catch( std::exception& e ) {
		ASSERT_STREQ( "dequeue() exception", e.what() );
	}
	
	EXPECT_EQ( initialMessage.what, dequeuedMessage.what );
	EXPECT_EQ( initialMessage.arg1, dequeuedMessage.arg1 );
	EXPECT_EQ( initialMessage.arg2, dequeuedMessage.arg2 );
	EXPECT_EQ( initialMessage.obj, dequeuedMessage.obj );
	
	// check if queue is empty now
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

TEST( MessageQueue, EnqueueGetFirstMessageDequeue )
{
	// first check if getFirstMessage will work
	ASSERT_NO_THROW( MessageQueue mq; );
	MessageQueue mq;
	std::string obj( "test object" );
	Message initialMessage( 1, 2, 3, &obj );
	Message getMessage;
	ASSERT_NO_THROW( mq.enqueue( initialMessage ) );
	
	try {
		bool gotFirstMessage = mq.getFirstMessage( getMessage );
		ASSERT_TRUE( gotFirstMessage );
	} catch( std::exception& e ) {
		ASSERT_STREQ( "getFirstMessage() exception", e.what() );
	}
	
	EXPECT_EQ( initialMessage.what, getMessage.what );
	EXPECT_EQ( initialMessage.arg1, getMessage.arg1 );
	EXPECT_EQ( initialMessage.arg2, getMessage.arg2 );
	EXPECT_EQ( initialMessage.obj, getMessage.obj );
	
	// check if message was not removed by dequeing it
	Message dequeuedMessage;	
	try {
		dequeuedMessage = mq.dequeue();
	} catch( std::exception& e ) {
		ASSERT_STREQ( "dequeue() exception", e.what() );
	}
	
	EXPECT_EQ( initialMessage.what, dequeuedMessage.what );
	EXPECT_EQ( initialMessage.arg1, dequeuedMessage.arg1 );
	EXPECT_EQ( initialMessage.arg2, dequeuedMessage.arg2 );
	EXPECT_EQ( initialMessage.obj, dequeuedMessage.obj );
	
	// check if queue is empty now
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

TEST( MessageQueue, DeleteMessages )
{
	// generate 3 messages, delete first two and dequeue
	ASSERT_NO_THROW( MessageQueue mq; );
	MessageQueue mq;
	std::string obj0( "test object 0" );
	std::string obj1( "test object 1" );
	std::string obj2( "test object 2" );
	Message msg0( 42, 2, 3, &obj0 ); // what == 42
	Message msg1( 42, 4, 5, &obj1 ); // what == 42
	Message msg2( 24, 6, 7, &obj2 ); // what == 24
	
	ASSERT_NO_THROW( mq.enqueue( msg0 ) );
	ASSERT_NO_THROW( mq.enqueue( msg1 ) );
	ASSERT_NO_THROW( mq.enqueue( msg2 ) );
	
	ASSERT_NO_THROW( mq.deleteSpecificMessages( 42 ) );
	
	// check if correct message is left (with 'what' == 24)
	Message dequeuedMessage;	
	try {
		dequeuedMessage = mq.dequeue();
	} catch( std::exception& e ) {
		ASSERT_STREQ( "dequeue() exception", e.what() );
	}
	
	EXPECT_EQ( msg2.what, dequeuedMessage.what );
	EXPECT_EQ( msg2.arg1, dequeuedMessage.arg1 );
	EXPECT_EQ( msg2.arg2, dequeuedMessage.arg2 );
	EXPECT_EQ( msg2.obj, dequeuedMessage.obj );
}

/// 
/// multi-threaded tests of MessageQueue

/// helper for pool of objects to feed Messages
std::vector< std::string > generateObjectPool( const size_t size )
{
	// create testing pool of objects, since Message does not manage object memory.
	// it will be removed automatically after all producers and consumers join
	std::vector< std::string > objPool;
	objPool.reserve( size );
	for ( int i = 0; i < size; ++i )
		objPool.emplace_back( std::string( "testObject " ) + std::to_string( i ) );
	return objPool;
}

TEST( MessageQueueProducerConsumer, Initialization )
{
	auto objPool = generateObjectPool( 10 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	EXPECT_NO_THROW( Producer p( mq, objPool ) );
	EXPECT_NO_THROW( Consumer c( mq, objPool.size() ) );
}

TEST( MessageQueueProducerConsumer, SingleProducerSingleConsumer )
{
	auto objPool = generateObjectPool( 10 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	ASSERT_NO_THROW( Producer p( mq, objPool ) );
	ASSERT_NO_THROW( Consumer c( mq, objPool.size() ) );
	Consumer c( mq, objPool.size() );
	Producer p( mq, objPool );
	
	std::thread tp( &Producer::run, &p );
	std::thread tc( &Consumer::run, &c );
	
	tp.join();
	tc.join();
	
	// check if queue is empty now
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

TEST( MessageQueueProducerConsumer, SingleProducerSingleConsumerOrder )
{
	auto objPool = generateObjectPool( 10 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	std::shared_ptr< std::vector< Message > > in( new std::vector< Message > );
	std::shared_ptr< std::vector< Message > > out( new std::vector< Message > );
	ASSERT_NO_THROW( Producer p( mq, objPool, std::chrono::milliseconds( 0 ), in ) );
	ASSERT_NO_THROW( Consumer c( mq, objPool.size(), std::chrono::milliseconds( 0 ), out ) );
	Consumer c( mq, objPool.size() );
	Producer p( mq, objPool );
	
	std::thread tp( &Producer::run, &p );
	std::thread tc( &Consumer::run, &c );
	
	tp.join();
	tc.join();
	
	// check if queue is empty now
	EXPECT_TRUE( isQueueEmpty( mq ) );
	
	// now check if order of things is correct
	ASSERT_EQ( in->size(), out->size() );
	for ( size_t i = 0; i < in->size(); ++i )
	{
		ASSERT_EQ( in->at( i ).what, out->at( i ).what );
		ASSERT_EQ( in->at( i ).arg1, out->at( i ).arg1 );
		ASSERT_EQ( in->at( i ).arg2, out->at( i ).arg2 );
		ASSERT_EQ( in->at( i ).obj, out->at( i ).obj );
	}
}

TEST( MessageQueueProducerConsumer, MultiProducerSingleConsumer )
{
	auto objPool = generateObjectPool( 100 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	ASSERT_NO_THROW( Producer p( mq, objPool ) );
	ASSERT_NO_THROW( Consumer c( mq, objPool.size() ) );
	Producer p0( mq, objPool );
	Producer p1( mq, objPool );
	Consumer c( mq, objPool.size() * 2 ); // must consume messages from both producers
	
	std::thread tp0( &Producer::run, &p0 );
	std::thread tp1( &Producer::run, &p1 );
	std::thread tc( &Consumer::run, &c );
	
	tp0.join();
	tp1.join();
	tc.join();
	
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

TEST( MessageQueueProducerConsumer, MultiProducerMultipleConsumer )
{
	auto objPool = generateObjectPool( 100 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	ASSERT_NO_THROW( Producer p( mq, objPool ) );
	ASSERT_NO_THROW( Consumer c( mq, objPool.size() ) );
	Producer p0( mq, objPool );
	Producer p1( mq, objPool );
	Producer p2( mq, objPool );
	Consumer c0( mq, objPool.size() );
	Consumer c1( mq, objPool.size() );
	Consumer c2( mq, objPool.size() );
	
	std::thread tp0( &Producer::run, &p0 );
	std::thread tp1( &Producer::run, &p1 );
	std::thread tp2( &Producer::run, &p2 );
	std::thread tc0( &Consumer::run, &c0 );
	std::thread tc1( &Consumer::run, &c1 );
	std::thread tc2( &Consumer::run, &c2 );
	
	tp0.join();
	tp1.join();
	tp2.join();
	tc0.join();
	tc1.join();
	tc2.join();
	
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

TEST( MessageQueueProducerConsumer, MultiProducerMultipleConsumerStress )
{
	auto objPool = generateObjectPool( 100000 );
	ASSERT_NO_THROW( MessageQueue mq );
	MessageQueue mq;
	ASSERT_NO_THROW( Producer p( mq, objPool ) );
	ASSERT_NO_THROW( Consumer c( mq, objPool.size() ) );
	std::vector< std::thread > producers;
	std::vector< std::thread > consumers;
	const size_t objPoolSize = objPool.size();
	for ( size_t i = 0; i < 500; ++i )
		producers.emplace_back( [ &mq, &objPool, i ]() { &Producer::run, Producer( mq, objPool, std::chrono::milliseconds( i ) ); } );
	for ( size_t i = 0; i < 1000; ++i )
		consumers.emplace_back( [ &mq, objPoolSize, i ]() { &Consumer::run, Consumer( mq, objPoolSize * 0.5, std::chrono::milliseconds( i ) ); } );
	
	for ( auto& producer : producers )
		producer.join();
	for ( auto& consumer : consumers )
		consumer.join();
	
	EXPECT_TRUE( isQueueEmpty( mq ) );
}

/// TODO test deletion in multiple producer/consumer context

int main( int argc, char **argv )
{
	testing::InitGoogleTest( &argc, argv );
	return RUN_ALL_TESTS();
}
