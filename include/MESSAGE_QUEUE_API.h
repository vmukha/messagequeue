// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file MESSAGE_QUEUE_API.h
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief API defining exports for library symbols
 */

#ifndef _MESSAGE_QUEUE_API_H_
#define _MESSAGE_QUEUE_API_H_

#ifndef MESSAGE_QUEUE_STATIC
	// shared library linking
	#ifdef _WIN32
		#ifdef MESSAGE_QUEUE_DLL
			#define MESSAGE_QUEUE_API __declspec( dllexport )
		#else
			#define MESSAGE_QUEUE_API __declspec( dllimport )
		#endif
	#else
		#define MESSAGE_QUEUE_API __attribute__((visibility("default")))
	#endif
#else
	// link statically
	#define MESSAGE_QUEUE_API
#endif

#endif // _MESSAGE_QUEUE_API_H_
