// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file MessageQueue.h
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Main interface of MessageQueue library
 */

#ifndef _MESSAGE_QUEUE_H_
#define _MESSAGE_QUEUE_H_


#include "MESSAGE_QUEUE_API.h"
#include "Message.h"

#include <memory>

/// forward declared implementation class
class MessageQueueImpl;

/// @class MessageQueue
/// @brief Thread-safe class representing a message queue
/// 
/// Implements a message queue class with thread-safe operations to enqueue and dequeue messages.
/// This message queue is supposed to be used in a classic concurrent producer-consumer problem.
/// 
class MessageQueue final
{
public:
	/// default constructor
	MESSAGE_QUEUE_API explicit MessageQueue();
	
	// rule of zero won't work due to pimpl, so using rule of five
	/// default non-virtual destructor
	MESSAGE_QUEUE_API ~MessageQueue();
	
	/// copying existing message queue is usually not what we want
	MESSAGE_QUEUE_API MessageQueue( const MessageQueue& ) = delete;
	
	/// copying existing message queue is usually not what we want
	MESSAGE_QUEUE_API MessageQueue& operator=( const MessageQueue& ) = delete;
	
	/// moves would be ill-formed in context of mutex
	MESSAGE_QUEUE_API MessageQueue( MessageQueue&& ) = delete;
	
	/// moves would be ill-formed in context of mutex
	MESSAGE_QUEUE_API MessageQueue& operator=( MessageQueue&& ) = delete;
	
	/// push new message into the queue
	/// @param[ in ] msg Message to be put into queue (reference to const, copied)
	/// 
	MESSAGE_QUEUE_API void enqueue( const Message& msg );
	
	/// push new message into the queue
	/// @param[ in ] msg Message to be put into queue (rvalue, moved)
	/// 
	MESSAGE_QUEUE_API void enqueue( Message&& msg );
	
	/// pop oldest message from the queue
	/// @returns dequeued message
	///
	/// If there is nothing to dequeue the method will block until something is enqueued.
	/// When something is enqueued, only one consumer which waits on dequeue() will be notified at a time.
	MESSAGE_QUEUE_API Message dequeue();
	
	/// get value of first element in the queue
	/// @param[ out ] msg Message to write the contents of first message to
	/// @returns false if queue is empty, true otherwise
	/// 
	/// The method does not wait until messages appear in the queue, but asks for currently first message.
	/// 
	/// NOTE: not giving reference or const-reference since it is error-prone is multi-threaded context.
	///       Also, modifying the first message of a message queue is not what classic producer-consumer want
	/// 
	MESSAGE_QUEUE_API bool getFirstMessage( Message& msg ) const;
	
	/// delete all elements with a specific "what" field from the queue
	/// @param what deletion criterion
	/// 
	/// This method does one-shot blocking deletion since when it is called we don't want 
	/// any other consumers to get these messages anymore
	/// 
	MESSAGE_QUEUE_API void deleteSpecificMessages( int what );
	
private:	
	/// pointer to implementation to hide implementation details (pimpl idiom)
	std::unique_ptr< MessageQueueImpl > m_pimpl;
};


#endif // _MESSAGE_QUEUE_H_
