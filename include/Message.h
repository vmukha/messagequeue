// Copyright (c) 2017 Viktor Mukha <mukha.victor@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*!
 * @file Message.h
 * @author Viktor Mukha <mukha.victor@gmail.com>
 * @brief Message class for transportation over message queue
 */

#ifndef _MESSAGE_H_
#define _MESSAGE_H_


#include "MESSAGE_QUEUE_API.h"

/// @struct Message
/// @brief Message to be stored in a message queue
/// 
/// NOTE: Client has to manage the lifetime of object 'obj'!
/// 
/// This class is fully implemented in header to avoid incompatibility problems.
/// If this class changes, clients must adapt and recompile.
///
struct MESSAGE_QUEUE_API Message final
{
	/// default constructor
	explicit Message()
	    : what( 0 )
	    , arg1( 0 )
	    , arg2( 0 )
	    , obj( nullptr )
	{}
	
	/// constructor for easier in-place message creation
	explicit Message( int _what, int _arg1, int _arg2, void* _obj )
	    : what( _what )
	    , arg1( _arg1 )
	    , arg2( _arg2 )
	    , obj( _obj )
	{}
	// NOTE: relying on compiler generated destructor, copy, and move methods

	int what;
	int arg1;
	int arg2;
	void* obj;
};


#endif // _MESSAGE_H_
